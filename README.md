# EM Slack Party Parroter

Add Party Parrots to your [Slack](#disclaimer) team's custom emoji library.

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=ErinMorelli_em-slack-party-parroter&metric=alert_status)](https://sonarcloud.io/dashboard?id=ErinMorelli_em-slack-party-parroter)

---

## Installation

1. Download this repository:

    ```
    git clone https://github.com/ErinMorelli/em-slack-party-parroter.git
    ```

2. Navigate into the folder created by step 1 and install the required python packages by running `pip`:

    ```
    pip install -r requirements.txt
    ```

3. Run the help command to make sure everything is working and to view the script's options:

    ```
    ./parroter.py --help
    ```


## Disclaimer

[Slack.com](https://www.slack.com/) is not affiliated with the maker of this product and does not endorse this product.
